//
//  WebserviceHelper.m
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import "WebserviceHelper.h"

@implementation WebserviceHelper
-(void)getJsonResponse:(NSString *)urlStr success:(void (^)(NSMutableArray *response))success failure:(void(^)(NSError* error))failure
{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                //NSLog(@"%@",data);
                                                if (error)
                                                    failure(error);
                                                else {
                                                    NSDictionary *json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                    //NSLog(@"%@",json);
                                                    success(json);
                                                }
                                            }];
    [dataTask resume];    // Executed First
}
@end
