//
//  WebserviceHelper.h
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebserviceHelper : NSObject
-(void)getJsonResponse:(NSString *)urlStr success:(void (^)(NSMutableArray *response))success failure:(void(^)(NSError* error))failure;
@end
