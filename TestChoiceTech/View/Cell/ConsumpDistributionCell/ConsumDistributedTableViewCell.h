//
//  ConsumDistributedTableViewCell.h
//  TestChoiceTech
//
//  Created by Admin on 17/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsumDistributedTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAmt;

@end
