//
//  InvestHeaderTableViewCell.h
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface InvestHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img_Product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Productname;
@end
