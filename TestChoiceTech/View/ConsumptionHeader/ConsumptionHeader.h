//
//  ConsumptionHeader.h
//  TestChoiceTech
//
//  Created by Admin on 17/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ConsumptonDelegate
-(void)updatedUserValue:(int)amount;
@end
@interface ConsumptionHeader : UITableViewHeaderFooterView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtfldEnterAmt;
@property (weak, nonatomic) IBOutlet UIView *vw_bottomline;
@property (weak, nonatomic) IBOutlet UILabel *lblAmtWords;
@property (nonatomic, weak) id  <ConsumptonDelegate> delegate;
@end
