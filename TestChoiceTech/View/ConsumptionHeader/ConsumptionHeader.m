//
//  ConsumptionHeader.m
//  TestChoiceTech
//
//  Created by Admin on 17/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import "ConsumptionHeader.h"

@implementation ConsumptionHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self formHeaderView];
}
-(void)formHeaderView{
    [self.vw_bottomline setBackgroundColor:[UIColor redColor]];
    UILabel *leftview = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 40)];
    leftview.text = @"₹ ";
    self.txtfldEnterAmt.leftView = leftview;
    self.txtfldEnterAmt.leftViewMode = UITextFieldViewModeAlways;
    self.txtfldEnterAmt.delegate = self;
    UIToolbar *toolb = [[UIToolbar alloc]init];
    toolb.barStyle = UIBarStyleDefault;
    [toolb setTranslucent:YES];
    toolb.backgroundColor = [UIColor darkGrayColor];
    [toolb sizeToFit];
    UIBarButtonItem *donebtn = [[UIBarButtonItem alloc]initWithTitle:@"done" style:UIBarButtonItemStylePlain target:self action:@selector(dismissKeyBoard)];
    UIBarButtonItem *spacebtn =[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolb setItems:@[spacebtn,donebtn]];
    [toolb setUserInteractionEnabled:YES];
    self.txtfldEnterAmt.inputAccessoryView = toolb;
    [self.txtfldEnterAmt addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}
-(void)dismissKeyBoard{
    [self.txtfldEnterAmt resignFirstResponder];
}
-(void)textFieldDidChange :(UITextField *)txtfld{
    if ([txtfld.text isEqualToString:@""]){
        self.lblAmtWords.text = @"";
        txtfld.textColor = [UIColor redColor];
    }else{
        int amount = [txtfld.text intValue];
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle: NSNumberFormatterSpellOutStyle];
        NSString *numberAsString = [[numberFormatter stringFromNumber:[NSNumber numberWithFloat:amount]] capitalizedString];
        self.lblAmtWords.text = numberAsString;
        if (amount >= 20000){
            txtfld.textColor = [UIColor blackColor];
            [self.vw_bottomline setBackgroundColor:[UIColor colorWithDisplayP3Red:39.0/255.0 green:174.0/255.0 blue:108.0/255 alpha:1.0]];
        }else{
             txtfld.textColor = [UIColor redColor];
            [self.vw_bottomline setBackgroundColor:[UIColor redColor]];
        }
    }
    [self UpdateTextFldAmt];
}

-(void)UpdateTextFldAmt{
    int amount = 0;
    if ((![self.txtfldEnterAmt.text isEqualToString:@""]) && [self.txtfldEnterAmt becomeFirstResponder]){
        amount = [self.txtfldEnterAmt.text intValue];
    }
    [self.delegate updatedUserValue:amount];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 9;
}

@end
