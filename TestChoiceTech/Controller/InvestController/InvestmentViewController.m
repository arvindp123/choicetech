//
//  InvestmentViewController.m
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "InvestmentViewController.h"
#import "Constant.h"
#import "InvestHeaderTableViewCell.h"
#import "ProductCardViewTableViewCell.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "DBWrapper.h"
#import "WebserviceHelper.h"
#import "TabBarController.h"

@interface InvestmentViewController()
@property(nonatomic,strong) NSString *hd_identifier;
@property(nonatomic,strong) NSString *product_identifier;
@property (nonatomic) NSMutableArray *investArr;
@end

@implementation InvestmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableviewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ((_investArr.count > 0) && (_investArr.count > 9)){
        return 10;
    }else{
        return self.investArr.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
    InvestHeaderTableViewCell *icell = [tableView dequeueReusableCellWithIdentifier:self.hd_identifier forIndexPath:indexPath
                                        ];
        icell.selectionStyle = UITableViewCellSelectionStyleNone;
        InvestmentModel * invets = _investArr[indexPath.row];
        icell.lbl_Productname.text = invets.title;
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:invets.url]];
        dispatch_async(dispatch_get_main_queue(), ^{
        icell.img_Product.image = [UIImage imageWithData:imageData];
        });
           return icell;
    }else{
        ProductCardViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.product_identifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
          [self addShadowToView:cell.contentView];
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return;
    }
    TabBarController *tab = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    [self.navigationController pushViewController:tab animated:true];
}
#pragma mark - Local & Remote Fetch
-(void)fetchProduct{
    if (![self connected]) {
        // Not connected
        [self fetchDataFromDb];
    } else {
        // Connected. Do some Internet stuff
        [self fetchDataFromRemote];
    }
}
-(void)fetchDataFromDb{
    self.investArr = [[DBWrapper alloc]fetchObject];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (_investArr.count > 0){
            [self.vw_Msg setHidden:true];
            [self.activity_loader stopAnimating];
            [self.tbl_Investment reloadData];
        }else{
            [self.vw_Msg setHidden:false];
            self.lbl_Msg.text = @"No data";
            [self.activity_loader stopAnimating];
        }
    });
}
-(void)fetchDataFromRemote{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    [web getJsonResponse:getData success:^(NSMutableArray *response) {
        if (response.count > 0){
            BOOL success = false;
            // save data to database
            success = [[DBWrapper alloc]insertObject:response];
            if (success == YES){
                // now fetch data from database
                [self fetchDataFromDb];
            }
        }else{
            // data is empty so fetch from database
            [self fetchDataFromDb];
        }
    } failure:^(NSError *error) {
    }];
}
#pragma mark - Methods
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
-(void)addShadowToView:(UIView*)view{
        // adding Shadow Effect to view
    view.layer.shadowOffset = CGSizeMake(0, 3);
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOpacity:0.5];
}
-(void)setupView{
    //Tableview intialize
    self.tbl_Investment.delegate = self;
    self.tbl_Investment.dataSource = self;
    self.hd_identifier = @"InvestHeaderTableViewCell";
    self.product_identifier = @"ProductCardViewTableViewCell";
    [self.tbl_Investment registerNib:[UINib nibWithNibName:self.hd_identifier bundle:nil] forCellReuseIdentifier:self.hd_identifier];
    [self.tbl_Investment registerNib:[UINib nibWithNibName:self.product_identifier bundle:nil] forCellReuseIdentifier:self.product_identifier];
    self.tbl_Investment.rowHeight = UITableViewAutomaticDimension;
    [self.tbl_Investment setEstimatedRowHeight:120];
    self.tbl_Investment.backgroundColor = tblBackColor;
    [self.vw_Msg setHidden:false];
    self.lbl_Msg.text = @"Please Wait";
    [self.activity_loader startAnimating];
    [self.activity_loader hidesWhenStopped];
    [self fetchProduct];
}
- (IBAction)btnInvestNowClicked:(id)sender {
}
@end
