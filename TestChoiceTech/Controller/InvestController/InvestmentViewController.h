//
//  InvestmentViewController.h
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvestmentViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tbl_Investment;
@property (weak, nonatomic) IBOutlet UIButton *btnInvestNow;
- (IBAction)btnInvestNowClicked:(id)sender;

//Error/Loader Msg View
@property (weak, nonatomic) IBOutlet UIView *vw_Msg;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity_loader;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Msg;


@end
