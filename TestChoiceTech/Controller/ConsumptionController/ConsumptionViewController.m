//
//  ConsumptionViewController.m
//  TestChoiceTech
//
//  Created by Admin on 17/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import "ConsumptionViewController.h"
#import "ConsumptionHeader.h"
#import "ConsumDistributedTableViewCell.h"

@interface ConsumptionViewController ()<ConsumptonDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tbl_consumption;
@property(nonatomic,strong) NSString *headerID;
@property(nonatomic,strong) NSString *distributCellID;
@property(nonatomic,strong) NSMutableArray *consumtionarr;
@property(nonatomic) int inputAmount;
@end

@implementation ConsumptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupViewNavigation];
    [self setupViewTableview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Tableview
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ConsumptionHeader * headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:_headerID];
    headerView.textLabel.text = @"test";
    headerView.delegate = self;
    return headerView;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ConsumDistributedTableViewCell *consumcell = [tableView dequeueReusableCellWithIdentifier:self.distributCellID forIndexPath:indexPath
                                                  ];
    NSDictionary *dict = self.consumtionarr[indexPath.row];
    consumcell.lblTitle.text = [dict valueForKey:@"name"];
    NSString *rupeeSymbol = @"₹ ";
    UIColor *defaultcolor = [UIColor colorWithRed:39.0/255.0 green: 174.0/255.0 blue:108.0/255 alpha:1.0];
    int amt = 0;
    if (indexPath.row == 0) {
         amt = self.inputAmount * 0.5;
    }
    else{
         amt = self.inputAmount * 0.25;
    }
    NSString *finalstr = [NSString stringWithFormat:@"%@ %d",rupeeSymbol,amt];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:finalstr];
    [string addAttribute:NSForegroundColorAttributeName value:defaultcolor range:NSMakeRange(0,1)];
    consumcell.lblAmt.attributedText = string;
    return consumcell;
}
#pragma mark - ConsumptionDelegate
-(void)updatedUserValue:(int)amount{
    self.inputAmount = amount;
    [self.tbl_consumption reloadData];
}
#pragma mark - Methods
-(NSMutableArray*)prepareData{
    NSDictionary *p1=@{@"name":@"Birla Sun Life Top 100 Fund Growth",@"amount" :@0};
    NSDictionary *p2=@{@"name":@"irla Sun Life Top 100 Fund Growth",@"amount" :@0};
    NSDictionary *p3=@{@"name":@"Kotak Emerging Equity Scheme-Growth",@"amount" :@0};
    NSMutableArray *arr = [@[p1,p2,p3] mutableCopy];
    return arr;
}
-(void)setupViewNavigation{
    //Navigation
    [[self navigationController] setNavigationBarHidden:false animated:false];
    [self.view setBackgroundColor:[UIColor grayColor]];
    self.navigationController.navigationBar.topItem.title = @"Consumption Story";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIButton* button = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    [button setTitle:@"Buy" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"Helvetica-Regular" size:17];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightBtn;
    self.tabBarController.navigationItem.rightBarButtonItem = rightBtn;
    self.consumtionarr = [self prepareData];
}
-(void)setupViewTableview{
    self.inputAmount = 0;
    //Tableview intialize
    _headerID = @"ConsumptionHeader";
    _distributCellID = @"ConsumDistributedTableViewCell";
    self.tbl_consumption.delegate = self;
    self.tbl_consumption.dataSource = self;
    [self.tbl_consumption registerNib:[UINib nibWithNibName:_headerID bundle:nil] forHeaderFooterViewReuseIdentifier:_headerID];
    [self.tbl_consumption registerNib:[UINib nibWithNibName:_distributCellID bundle:nil] forCellReuseIdentifier:_distributCellID];
    [self.tbl_consumption setRowHeight:UITableViewAutomaticDimension];
    [self.tbl_consumption setEstimatedRowHeight:40];
    [self.tbl_consumption setSectionHeaderHeight:UITableViewAutomaticDimension];
    [self.tbl_consumption setEstimatedSectionHeaderHeight:420];
    self.tbl_consumption.backgroundColor = [UIColor colorWithRed:0.9294 green:0.9294 blue:0.9294 alpha:1.0f];
    //Tableview  footer
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.tbl_consumption.tableFooterView = footerView;
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:39/255.0 green:174/255.0 blue:108/255.0 alpha:1.0];
}
@end
