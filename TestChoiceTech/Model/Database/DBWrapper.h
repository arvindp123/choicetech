//
//  DBWrapper.h
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InvestmentModel.h"
#import <sqlite3.h>
@interface DBWrapper : NSObject
@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *sqldatabase;
-(BOOL)insertObject:(NSMutableArray*)data;
-(NSMutableArray*)fetchObject;
- (void)copyDatabaseIfNeeded;
-(void)createTable;
-(void)deleteTable;
@end
