//
//  DBWrapper.m
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "DBWrapper.h"

@implementation DBWrapper
#pragma mark - Database Manupilation
-(BOOL)insertObject:(NSMutableArray*)data{
    BOOL result = false;
    sqlite3 *database;
    sqlite3_stmt    *statement;
    if (sqlite3_open([[self getDBPath] UTF8String], &database) == SQLITE_OK){
        NSString *querySQL = @"INSERT OR REPLACE INTO InvestmentTable( pid, albumId,  title, url, thumbnailUrl) VALUES(? , ? , ? , ? , ?);";
        for (NSDictionary *dict in data){
            InvestmentModel *invest = [[InvestmentModel alloc]  initWithDict:dict];
            const char *query_stmt = [querySQL UTF8String];
            if (sqlite3_prepare_v2(database,
                                   query_stmt, -1, &statement, NULL) == SQLITE_OK)
            {
                sqlite3_bind_int(statement, 1, invest.pid);
                sqlite3_bind_int(statement, 2, invest.albumId);
                sqlite3_bind_text(statement, 3, invest.title.UTF8String, -1,nil);
                sqlite3_bind_text(statement, 4, invest.url.UTF8String, -1,nil);
                sqlite3_bind_text(statement, 5, invest.thumbnailUrl.UTF8String, -1,nil);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    result = true;
                }else{
                    result = false;
                }
            }
            sqlite3_finalize(statement);
            //NSLog(@"%d",invest.albumId);
        }
    }
    sqlite3_close(database);
    return result;
}
-(BOOL)insertSingleObject:(NSDictionary*)data{
    BOOL result = false;
    sqlite3 *database;
    sqlite3_stmt    *statement;
    if (sqlite3_open([[self getDBPath] UTF8String], &database) == SQLITE_OK){
        NSString *querySQL = @"INSERT OR REPLACE INTO InvestmentTable( pid, albumId,  title, url, thumbnailUrl) VALUES(? , ? , ? , ? , ?);";
            InvestmentModel *invest = [[InvestmentModel alloc]  initWithDict:data];
            const char *query_stmt = [querySQL UTF8String];
            if (sqlite3_prepare_v2(database,
                                   query_stmt, -1, &statement, NULL) == SQLITE_OK)
            {
                sqlite3_bind_int(statement, 1, invest.pid);
                sqlite3_bind_int(statement, 2, invest.albumId);
                sqlite3_bind_text(statement, 3, invest.title.UTF8String, -1,nil);
                sqlite3_bind_text(statement, 4, invest.url.UTF8String, -1,nil);
                sqlite3_bind_text(statement, 5, invest.thumbnailUrl.UTF8String, -1,nil);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    result = true;
                }else{
                    result = false;
                }
            }
            sqlite3_finalize(statement);        
    }
    sqlite3_close(database);
    return result;
}
-(NSMutableArray*)fetchObject{
    NSMutableArray *resultarr = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt    *statement;
    NSString *dbpath = [self getDBPath];
    if (sqlite3_open([dbpath UTF8String], &database) == SQLITE_OK)
    {
        NSString *querySQL = @"Select * from InvestmentTable";
        const char *query_stmt = [querySQL UTF8String];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK){
                  while (sqlite3_step(statement) == SQLITE_ROW){
                      InvestmentModel *invest = [[InvestmentModel alloc]initWithData:(int)sqlite3_column_int(statement, 1) pid:(int)sqlite3_column_int(statement, 0) title:[[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)] url:[[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)] thumbnailUrl:[[NSString alloc]initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)]];
                      [resultarr addObject:invest];
                  }
              sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
    return  resultarr;
}
#pragma mark - Check Database
- (void)copyDatabaseIfNeeded
{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:[self getDBPath]];
    if(success)
    {
        return;// If exists, then do nothing.
    }
    //Get DB from bundle & copy it to the doc dirctory.
    NSString *databasePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Consumption.sqlite"];
    [fileManager copyItemAtPath:databasePath toPath:[self getDBPath] error:nil];
}
//Check DB in doc directory.
- (NSString *)getDBPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    return [documentsDir stringByAppendingPathComponent:@"Consumption.sqlite"];
}
-(void)createTable{
    const char *dbpath = [[self getDBPath] UTF8String];
    if (sqlite3_open(dbpath, &_sqldatabase) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt =
        "CREATE TABLE IF NOT EXISTS InvestmentTable (pid INTEGER PRIMARY KEY, albumId INTEGER,title TEXT, url TEXT, thumbnailUrl TEXT)";
        if (sqlite3_exec(self.sqldatabase, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            // @"Failed to create table";
        }
        sqlite3_close(_sqldatabase);
    }
}

@end
