//
//  InvestmentModel.h
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InvestmentModel : NSObject
@property (nonatomic,strong) NSString *title, *thumbnailUrl, *url;
@property (nonatomic,assign) int albumId,pid;

-(id)initWithDict
:(NSDictionary*)jsonObject;

-(id)initWithData:(int)albumId pid:(int)pid title:(NSString*)title url:(NSString*)url thumbnailUrl:(NSString*)thumbnailUrl;
@end
