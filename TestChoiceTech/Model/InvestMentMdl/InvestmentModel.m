//
//  InvestmentModel.m
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import "InvestmentModel.h"

@implementation InvestmentModel

-(id)initWithDict:(NSDictionary*)jsonObject
{
    self = [super init];
    if(self)
    {
        _albumId = [[jsonObject objectForKey:@"albumId"] intValue];
        _pid = [[jsonObject objectForKey:@"id"] intValue];
        _title = [jsonObject objectForKey:@"title"];
        _url = [jsonObject objectForKey:@"url"];
        _thumbnailUrl =[jsonObject objectForKey:@"thumbnailUrl"];
    }
    return self;
}
-(id)initWithData:(int)albumId pid:(int)pid title:(NSString*)title url:(NSString*)url thumbnailUrl:(NSString*)thumbnailUrl{
    self = [super init];
    if(self)
    {
        _albumId = albumId;
        _pid = pid;
        _title = title;
        _url = url;
        _thumbnailUrl = thumbnailUrl;
    }
    return self;
}
@end
